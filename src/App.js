import AppNavBar from './components/AppNavBar';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import { Navigate } from 'react-router-dom';
import './App.css';
import Home from './Pages/Home';
import Courses from './Pages/Courses';
import Register from './Pages/Register';
import Login from './Pages/Login';
import Logout from './Pages/Logout';
import Error from './Pages/Error';

function App() {
  return (
    <Router>
      <AppNavBar />
      <Container>
        <Routes>
          <Route exac path='/' element={<Home />} />
          <Route exac path='/courses' element={<Courses />} />
          <Route exac path='/register' element={<Register />} />
          <Route exac path='/login' element={<Login />} />
          <Route exac path='/logout' element={<Logout />} />
          <Route path='*' element={<Error />} />
        </Routes>
      </Container>
    </Router>
  );
}

export default App;

/* 
  Reading List:
	>>https://reactrouter.com/docs/en/v6/getting-started/overview
	>>https://upmostly.com/tutorials/react-filter-filtering-arrays-in-react-with-examples
	>>https://bobbyhadz.com/blog/react-export-multiple-functions#:~:text=Use%20named%20exports%20to%20export,necessary%20in%20a%20single%20file.
	>>https://reactjs.org/docs/lists-and-keys.html#keys
	>>https://stackoverflow.com/questions/32128978/react-router-no-not-found-route
	>>https://stackoverflow.com/questions/69868956/how-to-redirect-in-react-router-v6
	>>https://stackoverflow.com/questions/45089386/what-is-the-best-way-to-redirect-a-page-using-react-router
*/
