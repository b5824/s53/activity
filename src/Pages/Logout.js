import { Navigate } from 'react-router-dom';

export default function Logout() {
  localStorage.clear();

  //Navigate is redirection
  return <Navigate to='/login' />;
}
