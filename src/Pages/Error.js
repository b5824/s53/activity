/* 
Activity s53
>> Create Error.js under pages folder
>> Create a route which will be accessed when a user enters an undefined route and display the Error page.
>> Refactor the Banner component to be useable for the Error page and the Home page.
>> Render the Error page in App.js
>> Send your outputs in Hangouts and link it in Boodle
*/

import Banner from '../components/Banner';

export default function Error() {
  return <Banner status='404' />;
}
