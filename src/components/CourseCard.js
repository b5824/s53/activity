import { Card, Button } from 'react-bootstrap';
import { useState, useEffect } from 'react';

export default function CourseCard({ courseProp }) {
  // console.log(props);
  //result: courseData[0] -- props holds the mock database
  // console.log(typeof props);
  //result: object
  // console.log(courseProp);
  const { name, description, price } = courseProp;
  // console.log(name);

  //Syntax: const [getter, setter] = useState(initialValueOfGetter)
  const [count, setCount] = useState(0);
  const [seats, countSeats] = useState(30);
  const [isOpen, setIsOpen] = useState(false);

  console.log(count, seats);
  /* 
    count++ - is a reassignment.
    count = count + 1;

    Which means we're reassigning the value. However with count + 1, we're only adding 1 to its
    original value
  */
  const enroll = () => {
    if (seats === 0 && count === 30) {
      // document.getElementById('btn').setAttribute('disabled', true);
      alert('No more seats available');
    } else {
      setCount(count + 5);
      countSeats(seats - 5);
      console.log(`Seats: ${seats}`);
    }
  };

  /* 
    useEffect is used if there's changes in the DOM. 

    React have 2 DOMS:
      currentDOM - the initial webpage it loads
      virtualDOM - It doesn't have physical manifestation in the webpage. It compares the currentDOM to the virtualDOM. It only reloads WHICHEVER component changes.
  */
  useEffect(() => {
    if (seats === 0) {
      // let a = document.getElementById('btn');
      // let b = a.getAttribute('class');
      // let c = b.setAttribute('class', 'btn-secondary');
      // console.log(c);
      setIsOpen(true);
    }
  }, [seats]);

  /* 
  Activity s51
>> Create a seats state in the CourseCard component and set the initial value to 30.
>> For every enrollment, deduct one to the seats.
    >> If the seats reaches zero.
    >> Do not add to the count.
    >> Do not deduct to the seats.
    >> Show an alert that says "no more seats".
>> Send your outputs in Hangouts and link it in Boodle
  */

  return (
    <Card className='cardHighlight p-3 mb-3 mt-3'>
      <Card.Body>
        <Card.Title>{name}</Card.Title>
        <Card.Subtitle>Description</Card.Subtitle>
        <Card.Text>{description}</Card.Text>
        <Card.Subtitle>Price:</Card.Subtitle>
        <Card.Text>PhP {price}</Card.Text>
        <Card.Text>Enrollees: {count}</Card.Text>
        <Button id='btn' variant='primary' onClick={enroll} disabled={isOpen}>
          Enroll
        </Button>
      </Card.Body>
    </Card>
  );
}
